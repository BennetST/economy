package net.madgamble.economy;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.madgamble.core.api.common.translation.T;

public class MoneyCommand implements CommandExecutor {

	private final EconomyPlugin plugin;
	
	public MoneyCommand(EconomyPlugin plugin) {
		this.plugin = plugin;
	}
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		Player p = (Player) cs;
		if (cmd.getName().equalsIgnoreCase("coins")) {
			double money = plugin.getEcon().getBalance((OfflinePlayer)cs) ;
			T.send("economy.money", p, money);
		}
		return true;
	}
}
