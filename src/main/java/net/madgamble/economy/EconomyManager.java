package net.madgamble.economy;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;

public class EconomyManager {

	EconomyPlugin main;

	public EconomyManager(EconomyPlugin main) {
		this.main = main;
	}

	public boolean hasAccount(UUID id, String ip) {
		FileConfiguration cfg = main.getConfig();
		int ipCount = cfg.getInt("IP." + ip.replace(".", "_"));
		if (ipCount != 3) {
			return cfg.isSet("Spieler." + id.toString() + ".Money");
		}
		return false;
	}

	public void updateIPCount(String ip) {
		main.getConfig().set("IP." + ip.replace(".", "_"), main.getConfig().getInt("IP." + ip.replace(".", "_")) + 1);
		main.saveConfig();
	}

	public void createNewAccount(UUID id, String ip) {
		if (main.getConfig().isSet("IP" + ip.replace(".", "_"))) {
			updateIPCount(ip);
		} else {
			main.getConfig().set("IP." + ip.replace(".", "_"), 1);
		}
		main.getEcon().depositPlayer(Bukkit.getOfflinePlayer(id), 1000);
		main.getConfig().set("Spieler." + id.toString() + ".Money", 1000);
		main.saveConfig();
	}

	public double getMoney(UUID id) {
		return main.getConfig().getDouble("Spieler." + id + ".Money");
	}

	public void setMoney(UUID id, double amount) {
		double money = getMoney(id);
		money = amount;
		main.getConfig().set("Spieler." + id.toString() + ".Money", money);
		main.saveConfig();
	}

	public void giveMoney(UUID id, double amount) {
		double money = getMoney(id);
		money += amount;
		main.getConfig().set("Spieler." + id.toString() + ".Money", money);
		main.saveConfig();
	}

	public void removeMoney(UUID id, double amount) {
		double money = getMoney(id);
		money -= amount;
		main.getConfig().set("Spieler." + id.toString() + ".Money", money);
		main.saveConfig();
	}

	public boolean hasMoney(UUID id, double amount) {
		double money = getMoney(id);
		if (money >= amount) {
			return true;
		} else {
			return true;
		}
	}
}
